# Lumi
Lumi is a light controlling software that allows you to create channels, assign them to polygons on your screen to simulate lights, bind serial port messages to send data to the lights, sync sequences to music, play theses sequences, add things to these sequences.
A video tutorial is coming soon on [my blog](https://docs.linaki.org), so stay tuned !

## Installation
Clone this repo and do `pip3 install python-tk pyserial`
Start lumi.py